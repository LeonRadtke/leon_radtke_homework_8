﻿using System;
namespace MyDungeon
{
	public class EntranceRoom
	{
		static public void EnterRoom()
		{
			Console.Clear();
			Console.WriteLine(" YOU ENTERED THE ENTRANCE HALL! \n\n");
			Console.WriteLine(" The entrance has collapsed and the room is full of rubble.\n");
			Console.WriteLine("(1) Move to the stone gate in the south.\n");
			Console.WriteLine("(2) Move to the crack in the wall in the east.\n");
			Console.WriteLine("(3) Move to the solid steel gate in the west.\n");

			ConsoleKey pressedKey = Console.ReadKey().Key;

			switch (pressedKey)
			{
				case (ConsoleKey.D1):
					{
						if (MyDungeon.m_heroClass == "Warblade")
						{

							Console.WriteLine("\n Your enormous strenght allows you to move the stone gate.\n");
							Console.ReadKey();
							CryptaRoom.EnterRoom();
						}
						else
						{

							Console.WriteLine("\n The stone gate is far to heavy for you to move, no matter your efforts.\n");
							Console.ReadKey();
							EnterRoom();
						}
					}
					break;


				case (ConsoleKey.D2):
					{
						if (MyDungeon.m_heroClass == "Lurker")
						{

							Console.WriteLine("\n You are flexible enough to fit through the crack.\n");
							Console.ReadKey();
							HollowRoom.EnterRoom();
						}
						else
						{

							Console.WriteLine("\n The crack is to small for you to fit through, no matter your efforts.\n");
							Console.ReadKey();
							EnterRoom();
						}
					}
					break;


				case (ConsoleKey.D3):
					{
						if (MyDungeon.m_heroClass == "Psionic")
						{
							Console.WriteLine("\n You use your mind powers on the runes on the steel gate, which makes it open on its own.\n");
							Console.ReadKey();
							PsionicRoom.EnterRoom();
						}
						else
						{
							Console.WriteLine("\n The steel gate is to heavy for you to move, no matter your efforts.\n");
							Console.ReadKey();
							EnterRoom();
						}
					}
					break;

				default:
					{
						Console.Clear();
						Console.WriteLine("INVALID INPUT - PLEASE TRY AGAIN ! \n\n");
						Console.ReadKey();
						EnterRoom();
						break;
					}
			}

		}
	}
}
