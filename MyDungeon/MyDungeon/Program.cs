﻿using System;

namespace MyDungeon
{
	class MyDungeon
	{
		public static string m_heroClass;

		static string playerName;

		public static void Main(string[] args)
		{
			Console.Title = "EXAMPLE DUNGEON";
			Console.BackgroundColor = ConsoleColor.Black;
			Console.ForegroundColor = ConsoleColor.White;
			Console.Clear();

			Console.WriteLine("Welcome to the Dungeon!\n\n");

			Console.WriteLine("Type in your name!");

			playerName = Console.ReadLine();

			Choice_HeroClass();
		}

		public static void Choice_HeroClass()
		{
			Console.WriteLine("Young adventurer, what is your profesion?\n\n" +
							   "(1) I am a master with the sword, a Warblade!\n" +
							   "(2) I am a stalker from the shadows, a Lurker !\n" +
							   "(3) I am a user of my brain powers, a Psionic!\n");

			ConsoleKey pressedKey = Console.ReadKey().Key;

			switch (pressedKey)
			{
				case (ConsoleKey.D1):
					{
						m_heroClass = "Warblade";
						break;
					}

				case (ConsoleKey.D2):
					{
						m_heroClass = "Lurker";
						break;
					}

				case (ConsoleKey.D3):
					{
						m_heroClass = "Psionic";
						break;
					}

				default:
					{
						Console.Clear();
						Console.WriteLine("INVALID INPUT - PLEASE TRY AGAIN ! \n\n");
						Choice_HeroClass();
						break;
					}
			}


			Console.WriteLine("\n" + playerName +" "+ m_heroClass + " is entering the dungeon!\n" +
							   "The entrance collapses behind you. If you want to get out, you must find the exit!\n");

			Console.ReadKey();

			EntranceRoom.EnterRoom();

		}


	}
}
	


